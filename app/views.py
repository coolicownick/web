from django.core.paginator import Paginator
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.



questions = [
    {
        "id": i,
        "title": f"Title {i}",
        "text": f"This is text for {i}",
        "tag": f"Tag{i}",
    } for i in range(12)
]
def index(request):
    paginator = Paginator(questions, 3)
    page = request.GET.get('page')
    content = paginator.get_page(page)
    return render(request,"index.html",{"questions":content})

def hot(request):
    paginator = Paginator(questions,3)
    page = request.GET.get('page')
    content = paginator.get_page(page)
    return render(request,"hot.html",{"questions":content,"type":"hot"})

def tag(request,tag):
    tagedQuestions = list(filter(lambda question: question["tag"] == tag , questions))
    paginator = Paginator(tagedQuestions,3)
    page = request.GET.get('page')
    content = paginator.get_page(page)
    return render(request,"index.html",{"questions":content,"type":"tag","tag":tag})

def new(request):
    return render(request,"newQuestion.html")

def question(request,num):
    return render(request,"question.html",{"question":questions[num]})

def settings(request):
    return render(request,"settings.html")

def login(request):
    return render(request,"login.html")

def register(request):
    return render(request,"registration.html")